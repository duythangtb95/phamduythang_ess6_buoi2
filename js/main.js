// data glasses
let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];

// hàm load kính lên trang
let renderGlasses = () => {
    let contentHtml = "";

    for (let i = 0; i < dataGlasses.length; i++) {
        contentHtml += `<img  class="col-4" src=${dataGlasses[i].src} id=${dataGlasses[i].id} >`;
    }
    document.getElementById("vglassesList").innerHTML = contentHtml;
}

renderGlasses()






// hàm show glass
let avatarGlass = document.getElementById("avatarGlass").innerHTML;

document.getElementById("G1").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[0].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[0].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[0].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[0].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[0].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[0].description}`;
}

document.getElementById("G2").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[1].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[1].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[1].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[1].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[1].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[1].description}`;

}

document.getElementById("G3").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[2].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[2].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[2].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[2].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[2].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[2].description}`;
}

document.getElementById("G4").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[3].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[3].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[3].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[3].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[3].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[3].description}`;
}

document.getElementById("G5").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[4].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[4].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[4].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[4].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[4].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[4].description}`;
}

document.getElementById("G6").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[5].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[5].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[5].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[5].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[5].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[5].description}`;
}

document.getElementById("G7").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[6].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[6].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[6].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[6].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[6].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[6].description}`;
}

document.getElementById("G8").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[7].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[7].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[7].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[7].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[7].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[7].description}`;
}

document.getElementById("G9").onclick = function () {
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatarGlass").style.display = "block";
    document.getElementById("avatarGlass").innerHTML = `<img src=${dataGlasses[8].virtualImg}>`;
    document.getElementById("glassName").innerHTML = `${dataGlasses[8].name}`;
    document.getElementById("glassBrand").innerHTML = `${dataGlasses[8].brand}`;
    document.getElementById("glassColor").innerHTML = `${dataGlasses[8].color}`;
    document.getElementById("glassPrice").innerHTML = `${dataGlasses[8].price}`;
    document.getElementById("glassDescription").innerHTML = `${dataGlasses[8].description}`;
}


function removeGlasses(a) {
    if (a == false) {
        document.getElementById("glassesInfo").style.display = "none";
        document.getElementById("avatarGlass").style.display = "none";
    } else {
        document.getElementById("glassesInfo").style.display = "block";
        document.getElementById("avatarGlass").style.display = "block";

    }

}